tasks = [];

function addTask() {
    let newTask = {
        task: document.forms['myForm']['task'].value,
        startDate: document.forms['myForm']['startDate'].value,
        endDate: document.forms['myForm']['endDate'].value,
        employee: document.forms['myForm']['employee'].value,
        status: document.forms['myForm']['status'].value,
        isRejected: false
    };
    tasks.push(newTask);
    drawList();
    document.forms['myForm'].reset();
}

function editTask(editingIndex) {
    if (document.getElementById('select' + editingIndex).value !== 'tanlang') {
        tasks[editingIndex].status = document.getElementById('select' + editingIndex).value;
    }
    drawList()
}

function rejectedTask(reIndex) {
    tasks[reIndex].status = 'rejected';
    tasks[reIndex].isRejected = true;
    drawList();
}

function deleteTask(deleteTask) {
    tasks.splice(deleteTask, 1);
    drawList();
}

function drawList() {
    document.getElementById('pendingTask').innerHTML = '';
    document.getElementById('doingTask').innerHTML = '';
    document.getElementById('doneTask').innerHTML = '';
    document.getElementById('rejectedTask').innerHTML = '';
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].status === 'pending') {
            document.getElementById('pendingTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task + (tasks[i].isRejected ? '<span class="badge badge-danger badge-pill">rejected</span>' : '') + '</h6>' +
                '<h6>' + 'Xodim: ' + tasks[i].employee + '</h6>' +
                '<h6>' + 'startDate: ' + tasks[i].startDate + '</h6>' +
                '<h6>' + 'endDate: ' + tasks[i].endDate + '</h6>' +
                '<select class="form-control" id="select' + i + '">' +
                '<option disabled selected>tanlang</option>' +
                '<option class="doi" value="doing">doing</option>' +
                '<option class="don" value="done">done</option>' +
                '</select>' +
                '<button type="button" class="btn btn-warning mt-3" onclick="editTask(' + i + ')">Edit</button>' +
                '<button type="button" class="btn btn-danger ml-3 mt-3" onclick="deleteTask(' + i + ')">Delete</button>' +
                '<hr class="mt-4 mb-3">' +
                '</div>'
        } else if (tasks[i].status === 'doing') {
            document.getElementById('doingTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task + (tasks[i].isRejected ? '<span class="badge badge-danger badge-pill">rejected</span>' : '') + '</h6>' +
                '<h6>' + 'Xodim: ' + tasks[i].employee + '</h6>' +
                '<h6>' + 'startDate: ' + tasks[i].startDate + '</h6>' +
                '<h6>' + 'endDate: ' + tasks[i].endDate + '</h6>' +
                '<select class="form-control" id="select' + i + '">' +
                '<option disabled selected>tanlang</option>' +
                '<option class="pen" value="pending">pending</option>' +
                '<option class="don" value="done">done</option>' +
                '</select>' +
                '<button type="button" class="btn btn-warning mt-3"  onclick="editTask(' + i + ')">Edit</button>' +
                '<button type="button" class="btn btn-danger ml-3 mt-3" onclick="deleteTask(' + i + ')">Delete</button>' +
                '<hr class="mt-4 mb-3">' +
                '</div>'

        } else if (tasks[i].status === 'done') {
            document.getElementById('doneTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task + (tasks[i].isRejected ? '<span class="badge badge-danger badge-pill">rejected</span>' : '') + '</h6>' +
                '<h6>' + 'Xodim: ' + tasks[i].employee + '</h6>' +
                '<h6>' + 'startDate: ' + tasks[i].startDate + '</h6>' +
                '<h6>' + 'endDate: ' + tasks[i].endDate + '</h6>' +
                '<button type="button" class="btn btn-danger mt-3" onclick="rejectedTask(' + i + ')">Rejected</button>' +
                '<hr class="mt-4 mb-3">' +
                '</div>'

        } else {
            document.getElementById('rejectedTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task + '</h6>' +
                '<h6>' + 'Xodim: ' + tasks[i].employee + '</h6>' +
                '<h6>' + 'startDate: ' + tasks[i].startDate + '</h6>' +
                '<h6>' + 'endDate: ' + tasks[i].endDate + '</h6>' +
                '<select id="select' + i + '" class="form-control">' +
                '<option disabled selected>tanlang</option>' +
                '<option class="doi" value="doing">doing</option>' +
                '<option class="pen" value="pending">pending</option>' +
                '</select>' +
                '<button type="button" class="btn btn-warning mt-3" onclick="editTask(' + i + ')">Edit</button>' +
                '<hr class="mt-4 mb-3">' +
                '</div>'
        }
    }
}
